@extends('landing.layout')


@section('landing-content')

<div class="main-content">
    <div class="header bg-primary pt-5 pb-7">
        <div class="container">
            <div class="header-body">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="pr-5">
                            <h1 class="display-2 text-white font-weight-bold mb-0">{{ __("School App") }}</h1>
                            <h2 class="display-4 text-white font-weight-light">
                                {{ __("Une belle application pour la gestion scolaire") }}.</h2>
                            <p class="text-white mt-4">
                                {{ $app_name }}
                                {{ __("combine parfaitement la pratique standard et une interface confortable pour gérer vos institutions") }}
                            </p>
                            <div class="mt-5">
                                <a href="#" class="btn btn-neutral my-2">
                                    {{ __('Tarification') }}
                                </a>
                                <a href="{{ route('login') }}" class="btn btn-default my-2">{{ __("Connexion") }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 d-none d-lg-block">
                        <img src="{{ asset('image/hero-home.svg') }}" class=" img-fluid" alt="{{ __("Home") }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="separator separator-bottom separator-skew zindex-100">
            <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1"
                xmlns="http://www.w3.org/2000/svg">
                <polygon style="fill: #f8f9fe" points="2560 0 2560 100 0 100"></polygon>
            </svg>
        </div>
    </div>


    <section class="py-6 pb-9">
        <div class="container">
            <div class="row justify-content-center text-center">
                <div class="col-md-8">
                    <h2 class="display-3">{{ __("Nos dernier articles") }}</h2>
                </div>
            </div>
        </div>
    </section>

    <section class="section section-lg pt-lg-0 mt--7">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="row">
                        @foreach ([1,2,3] as $item)
                        <div class="col-lg-4">
                            <div class="card">
                                <img class="card-img-top"
                                    src="https://demos.creative-tim.com/argon-dashboard-pro/assets/img/theme/img-1-1000x600.jpg"
                                    alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">Card title that wraps to a new line</h5>
                                    <p class="card-text">This is a longer card with supporting text below as a natural
                                        lead-in to additional content. This content is a little bit longer.</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

                <div class="col-lg-12 mt-5 d-flex justify-content-center">
                    <a href="#" class="btn btn-primary">
                        {{ __("Voir plus") }}
                    </a>
                </div>
            </div>
        </div>
    </section>

</div>

@endsection