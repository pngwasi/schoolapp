@extends('landing.layout')


@section('landing-content')

<div class="container mt-8 pb-5">
    <div class="row justify-content-center">
        <div class="col-lg-5 col-md-7">
            @yield('auth-content')
        </div>
    </div>
</div>
@endsection