// @ts-check
import prefresh from '@prefresh/vite'
import path from 'path'

const root = "./assets";

const watchResourceViews = () => {
    return {
        name: 'watch-resource-views',
        /**
         * @param { import('vite').ViteDevServer  } server 
         */
        configureServer(server) {
            const { watcher, ws } = server
            watcher.add(path.resolve(root, "../resources/**/*.blade.php"));
            watcher.on("change", function (path) {
                if (path.endsWith(".blade.php")) {
                    ws.send({
                        type: "full-reload",
                        path,
                    });
                }
            });
        },
    }
}

const assetsFileExport = () => {
    let originServer
    const test = /\.(svg|png|jp(e)?g|gif|woff|woff2|eot|ttf|otf)$/

    return {
        name: 'assets-file-export',
        /**
         * @param {import('vite').ViteDevServer} server 
         */
        configureServer(server) {
            server.app.use((req, res, next) => {
                originServer = req.headers.host
                next()
            })
        },
        transform(code, id) {
            if (test.test(id)) {
                const getPath = code.split('')
                    .reduce((acc, c) => {
                        if (!acc.length && c === '"') {
                            acc += c
                        } else if (acc.length && c.length) {
                            acc += c
                        }
                        return acc
                    }, '')

                const originPath = `"${originServer || ''}${getPath.substr(1)}`

                return {
                    code: code.replace(getPath, originPath),
                    map: null
                }
            }
        }
    }
}


/**
 * @type { import('vite').UserConfig }
 */
const config = {
    alias: {
        "/@/": path.resolve(__dirname, 'assets/'),
        react: "preact/compat",
        "react-dom": "preact/compat",
    },
    optimizeDeps: {
        include: [
            "gia/Component",
            "gia/eventbus"
        ],
    },
    root,
    plugins: [
        prefresh(),
        watchResourceViews(),
        assetsFileExport()
    ]
};

module.exports = config;