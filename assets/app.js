//@ts-nocheck
import Swup from 'swup';
import SwupGiaPlugin from '@swup/gia-plugin';
import SwupProgressPlugin from '@swup/progress-plugin';
import SwupMetaTagsPlugin from 'swup-meta-tags-plugin';
import components from './components/components';
import $ from 'jquery'

window.$ = window.jQuery = $
import 'bootstrap'
import './lib/argon/js/argon'

const swup = new Swup({
    cache: false,
    plugins: [
        new SwupGiaPlugin({ components: components, log: process.env.NODE_ENV === "development" }),
        new SwupProgressPlugin(),
        new SwupMetaTagsPlugin()
    ]
});
