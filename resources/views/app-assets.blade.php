@if (app()->environment() === "local")

    <script type="module" src="{{ env('DEV_SERVER') }}/@vite/client"></script>
    @foreach (explode(',', env('DEV_SERVER_ENTRIES')) as $entry)
        @if ($entry)
            <script type="module" src="{{ env('DEV_SERVER') }}/{{ $entry }}" defer></script>
        @endif
    @endforeach

@else
    <link href="{{ mix('main.css', 'assets') }}" rel="stylesheet">
    <script src="{{ mix('manifest.js', 'assets') }}" defer></script>
    <script src="{{ mix('vendor.js', 'assets') }}" defer></script>
    <script src="{{ mix('app.js', 'assets') }}" defer></script>
@endif