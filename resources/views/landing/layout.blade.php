@extends('app')


@section('content')

@include('landing.navbar')

@yield('landing-content')

@include('landing.footer')

@endsection