const mix = require('laravel-mix');
const path = require('path')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('assets/app.js', 'app.js')
    .sass('assets/app.scss', 'main.css')
    .extract(['jquery', 'bootstrap', 'popper.js'])
    .options({
        terser: {
            extractComments: false,
        }
    })
    .webpackConfig({
        resolve: {
            alias: {
                "/@": path.resolve(__dirname, 'assets/'),
            },
        },
        output: {
            publicPath: '/assets/',
        }
    })
    .setPublicPath('public/assets/')
    .setResourceRoot('/assets/')
    .version()
    .sourceMaps(false)
    .disableNotifications();