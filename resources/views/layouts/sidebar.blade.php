<nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-dark shadow-none bg-primary"
    id="sidenav-main">
    <div class="scrollbar-inner">
        <div class="sidenav-header">
            <a class="navbar-brand text-center" href="#">
                <span class="h1 font-weight-bold text-light">
                    {{ $app_name }}
                </span><br>
                <small class="text-muted text-light" style="font-size: .9rem">
                    {{ __($label ?? '') }}
                </small>
            </a>
        </div>
        <div class="navbar-inner mt-4">
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <ul class="navbar-nav">
                    @foreach ($menus ?? [] as $k => $link)
                    <li class="nav-item">
                        <a class="nav-link {{ $k == 0 ? 'active': '' }}" href="{{ $link['route'] }}">
                            <i class="material-icons text-default">
                                label
                            </i>
                            <span class="nav-link-text">{{ __($link['name']) }}</span>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</nav>