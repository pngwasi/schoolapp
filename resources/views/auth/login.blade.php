@extends('auth.layout')

@section('title', __("Se connecter").' | ')

@section('auth-content')
<div class="card bg-secondary shadow-sm border mb-0">
    <div class="card-body px-lg-5 py-lg-5">
        <div class="text-center text-muted mb-4">
            <small>{{ __("Connectez-vous avec vos identifiants") }}</small>
        </div>
        <form role="form">
            <div class="form-group mb-3">
                <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <span class="material-icons">
                                email
                            </span>
                        </span>
                    </div>
                    <input class="form-control" placeholder="{{ __("Email") }}" type="email">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <span class="material-icons">
                                lock
                            </span>
                        </span>
                    </div>
                    <input class="form-control" placeholder="{{ __("Mot de passe") }}" type="password">
                </div>
            </div>
            <div class="custom-control custom-control-alternative custom-checkbox">
                <input class="custom-control-input" id=" customCheckLogin" type="checkbox">
                <label class="custom-control-label" for=" customCheckLogin">
                    <span class="text-muted">{{ __("Garder ma session permanente") }}</span>
                </label>
            </div>
            <div class="text-center">
                <button type="button" class="btn btn-default my-4">
                    {{ __("Connexion") }}
                </button>
            </div>
        </form>
    </div>
</div>
<div class="row mt-3">
    <div class="col-6">
        <a href="{{ route('password.request') }}"><small>{{ __("Mot de passe oublié") }}?</small></a>
    </div>
    <div class="col-6 text-right">
        <a href="{{ route('register') }}"><small>{{ __("Créer un nouveau compte") }}</small></a>
    </div>
</div>
@endsection